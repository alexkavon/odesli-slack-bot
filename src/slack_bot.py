from odesli.Odesli import Odesli

odesli = Odesli()
result = odesli.getByUrl('https://www.pandora.com/artist/thundercat/it-is-what-it-is-explicit/funny-thing/TRggmK5Khd7Jj4c')

for key, provider in result.songsByProvider.items():
    print(key + '\n' + '----------')
    for serviceName, serviceUrl in provider.linksByPlatform.items():
        print(serviceName + ': ' + serviceUrl + '\n')
